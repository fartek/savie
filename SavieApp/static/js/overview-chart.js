$(window).resize(function()
{
    drawChart($("#account-id").val());
});

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
//google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart(account_id)
{
    var this_month = Date.create().format("{M}");
    var today = Date.create();
    var query = window.location.search;

    var regex = /timeframe=([a-zA-Z0-9-_#]+)/gi;
    var results = regex.exec(query);
    var result = results ? results[results.length-1] : null;
    var since = "";

    if(result == "last-month")
    {
        $("#graph-since-button-text").text("Since last month");
        $("#graph-since-options-list > li").removeClass("view-hidden");
        $("#graph-since-option-"+result).parent().addClass("view-hidden");
        since += "&since-months=1";
    }
    else if(result == "last-3-months")
    {
        $("#graph-since-button-text").text("Since last 3 months");
        $("#graph-since-options-list > li").removeClass("view-hidden");
        $("#graph-since-option-"+result).parent().addClass("view-hidden");
        since = "&since-months=3";
    }
    else if(result == "last-year")
    {
        $("#graph-since-button-text").text("Since last year");
        $("#graph-since-options-list > li").removeClass("view-hidden");
        $("#graph-since-option-"+result).parent().addClass("view-hidden");
        since = "&since-months=12";
    }
    else if(result == "all")
    {
        $("#graph-since-button-text").text("Since your registration");
        $("#graph-since-options-list > li").removeClass("view-hidden");
        $("#graph-since-option-"+result).parent().addClass("view-hidden");
        since = "&since-months=all";
    }

    $.getJSON( "/api/v1/categories?money-account-id="+account_id + since, function( result ) {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Category name');
        data.addColumn('number', 'Sum of expenses');

        var data_rows = [];

        for (var i = 0; i < result.categories.length; i++) {
            var category = result.categories[i];
            var category_name = category.name;

            var total_category_expenses = 0;


            console.log(category_name);

            for(var j=0;j<category.expenses.length;j++)
            {
                var expense = category.expenses[j];
                var expense_date = Date.create(expense.created_at);

                console.log("    " + expense.name);

                total_category_expenses += expense.value;
            }

            data_rows.push([category_name, total_category_expenses]);
        }

        data.addRows(data_rows);

        w = document.getElementById("overview-graph-container").offsetWidth-30;

        // Set chart options
        var options = {'title':'Division of expenses by category',
            'width':w,
            'height':w/1.5};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    });
}
