var settings = {
    "email_valid" : true,
    "password_valid" : true
};

(function()
{
    function validateEmailFormat(email)
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function validateEmails()
    {
        //Check 'new email' (format)
        if(validateEmailFormat($("#settings-new-email").val()))
        {
            settings.email_valid = true;

            $("#settings-new-email-status").removeClass("view-block")
                .addClass("view-hidden");
        }
        else
        {
            settings.email_valid = false;

            $("#settings-new-email-status").removeClass("view-hidden")
                .addClass("view-block");
        }

        //Check 'new email' confirmation (if it matches the 'new email')
        if($("#settings-new-email-confirm").val() != $("#settings-new-email").val())
        {
            $("#settings-new-email-confirm-status").removeClass("view-hidden")
                .addClass("view-block");

            settings.email_valid = false;
        }
        else
        {
            $("#settings-new-email-confirm-status").removeClass("view-block")
                .addClass("view-hidden");
        }
    }

    function validatePasswords()
    {
        settings.password_valid = true;

        // If 'current password' is empty and 'new password' is not empty
        if($("#settings-new-password").val() != "" && $("#settings-password").val() == "")
        {
            $("#settings-password-status").text("You must enter your current password")
                .removeClass("view-hidden")
                .addClass("view-block");

            settings.password_valid = false;
        }

        //If 'current password' is set but a 'new password' is not
        if($("#settings-password").val() != "" && $("#settings-new-password").val() == "")
        {
            $("#settings-new-password-confirm-status").removeClass("view-hidden")
                .addClass("view-block")
                .text("Your new password must contain at least one character");
            settings.password_valid = false;
        }
        //If the 'new password' and 'new password confirmation' values do not match
        else if($("#settings-new-password").val() != $("#settings-new-password-confirm").val())
        {
            $("#settings-new-password-confirm-status").removeClass("view-hidden")
                .addClass("view-block")
                .text("The passwords do not match");
            settings.password_valid = false;
        }
        else
        {
            $("#settings-new-password-confirm-status").removeClass("view-block")
                .addClass("view-hidden")
        }
    }

    function onRemoveMoneyAccount(e)
    {
        if($("#settings-money-accounts > a").length <= 1)
        {
            alert("You must always have at least one money account.")
        }
        else
        {
           var confirm_delete = confirm("Are you sure you want to remove this money account? Everything associated with it, including categories and expenses will be removed!");
            if(confirm_delete)
            {
                var this_item = $(e.target);
                var item_text = this_item.text().trim().toLowerCase();

                $('#money-account-inputs input[value='+item_text+']').remove();

                this_item.remove();
                alert("To actually submit the changes don't forget to click the 'submit' button at the bottom.");
            }
        }

    }

    function hasWhiteSpace(s) {
      return /\s/g.test(s);
    }

    function addCheckerEvents()
    {
        $("#settings-money-accounts > a.list-group-item").click(onRemoveMoneyAccount);

        $("#settings-add-money-account").click(function(e){

            if($("#money-account-name").val().trim() == "")
            {
                alert("Money account name cannot be empty");
            }
            else if(hasWhiteSpace($("#money-account-name").val().trim()))
            {
                alert("Money account name cannot have white spaces");
            }
            else
            {
                var duplicate = false;
                $('#settings-money-accounts > a.list-group-item').each(function() {
                    if ($(this).text().toLowerCase().trim() == $("#money-account-name").val().toLowerCase().trim())
                    {
                        duplicate = true;
                    }
                });

                if(duplicate)
                {
                    alert("A money account with this name already exists");
                }
                else
                {
                    $("#settings-money-accounts").append($('<a href="#" class="list-group-item"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <strong>'+
                        $("#money-account-name").val().trim().capitalize(true)+'</strong></a>').click(onRemoveMoneyAccount));
                    $('#money-account-inputs').append($('<input name="money-account-names[]" type="hidden" class="money-account-name" value="'+ $("#money-account-name").val().trim().toLowerCase()+'">'));

                    $("#money-account-name").val("");
                }
            }

        });

        $(".status-hide").click(function()
        {
            $(this).parent().parent().removeClass("view-block").addClass("view-hidden");
        });

        $("#settings-submit-form").click(function(e)
        {
            e.preventDefault();
            if(settings.email_valid && settings.password_valid)
            {
                $("#settings-form").submit();
            }
            else
            {
                alert("Please fill all the required fields.");
            }
        });

        $("#settings-new-email").focusin(function()
            {
                $("#settings-new-email-confirm-label").removeClass("view-hidden")
                    .addClass("view-block");

                $("#settings-new-email-confirm").removeClass("view-hidden")
                    .addClass("view-block");
            })
            .focusout(function()
            {
                if($(this).val() == "") // if "New email" is empty
                {
                    settings.email_valid = true;

                    //confirm new email and the it's label
                    $("#settings-new-email-confirm-label").removeClass("view-block")
                        .addClass("view-hidden");
                    $("#settings-new-email-confirm").removeClass("view-block")
                        .addClass("view-hidden").val("");

                    //statuses
                    $("#settings-new-email-status").removeClass("view-block")
                        .addClass("view-hidden");
                    $("#settings-new-email-confirm-status").removeClass("view-block")
                        .addClass("view-hidden");
                }
            })
            .change(function()
            {
                validateEmails();
            });

        $("#settings-new-email-confirm").change(function()
        {
            validateEmails();
        });

        // Check passwords
        $("#settings-password").change(function()
        {
            validatePasswords();
        });

        $("#settings-new-password").focusin(function()
            {
                $("#settings-new-password-confirm-label").removeClass("view-hidden")
                    .addClass("view-block");
                $("#settings-new-password-confirm").removeClass("view-hidden")
                    .addClass("view-block");

                validatePasswords();
            })
            .focusout(function()
            {
                if($(this).val() == "")
                {
                    $("#settings-new-password-confirm-label").removeClass("view-block")
                        .addClass("view-hidden");
                    $("#settings-new-password-confirm").removeClass("view-block")
                        .addClass("view-hidden").val("");
                }
                validatePasswords();
            })
            .change(function()
            {
                validatePasswords();
            });

        $("#settings-new-password-confirm").change(function()
        {
            validatePasswords()
        });
    }

    $(document).ready(function()
    {
        addCheckerEvents();
    });
}());
