(function(){
    var geoKey = "AIzaSyAPoqd8w8m2oXMsGEquYVEUK-DNRAzDs4Y";

    var login = {
        type : "login",
        password_valid : false,
        username_valid : false,
        email_valid : false
    };

    function submitLogin()
    {
        $("#user-form").submit();
    }

    function validateEmailFormat(email)
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function checkEmail()
    {
        if(!validateEmailFormat($("#login-email").val()) || $("#login-email").val() == "")
        {
            $("#login-status").text("Invalid email format")
                .removeClass("view-hidden")
                .addClass("view-block");
            login.email_valid = false;
        }
        else
        {
            login.email_valid = true;
        }
    }

    function checkLogin()
    {
        var user = document.getElementById("login-username").value;
        var password = document.getElementById("login-password").value;

        if(login.type == "login")
        {
            $("#user-form").submit();
        }
        else if(login.type == "register")
        {
            console.log("checking username");

            checkUsername();
            if(!login.username_valid)
            {
                return;
            }

            console.log("Checking password");

            checkPasswords();
            if(!login.password_valid)
            {
                return;
            }

            console.log("Checking email");
            checkEmail();
            if(!login.email_valid)
            {
                return;
            }

            //Disable buttons
            $("#login-submit-btn").prop('disabled', true)
                .text("Registering...");
            $("#login-tab-login").unbind("click");
            $("#login-tab-register").unbind("click");

            if (navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(function(position)
                {
                    $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + ","+ position.coords.longitude + "&result_type=country&language=en&key=" + geoKey, function( data ) {
                        if(data.results.length < 1)
                        {
                            submitLogin();
                            return;
                        }

                        var countryName = data.results[data.results.length-1].formatted_address;
                        $.getJSON("https://restcountries.eu/rest/v1/name/" + countryName, function(data)
                        {
                            if(data.lenght < 1)
                            {
                                submitLogin();
                                return;
                            }
                            var currency = data[data.length-1].currencies[0];
                            $("#currency").val(currency);
                            submitLogin();
                        });
                    });
                },
                function (error)
                {
                    if (error.code == error.PERMISSION_DENIED)
                        submitLogin();
                });
            } else {
               submitLogin()
            }
        }
    }
    function openLogin()
    {
        $("#form-type").val("login");

        $("#login-submit-btn").text("Log in");
        $("#login-reset-password").text("Reset my password")
            .removeClass("view-hidden")
            .addClass("view-block");
        $("#login-password-label").removeClass("view-hidden")
            .addClass("view-nlock");
        $("#login-password").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-tab-login").css("font-weight", "bold");
        $("#login-tab-register").css("font-weight", "normal");
        $("#login-password-confirm-label").removeClass("view-block")
            .addClass("view-hidden");
        $("#login-password-confirm").removeClass("view-block")
            .addClass("view-hidden");
        $("#login-email-label").removeClass("view-block")
            .addClass("view-hidden");
        $("#login-email").removeClass("view-block")
            .addClass("view-hidden");
        $("#login-status").removeClass("view-block")
            .addClass("view-hidden");
        login.type = "login";
    }

    function openRegister()
    {
        $("#form-type").val("register");

        $("#login-submit-btn").text("Register");
        $("#login-reset-password").text("Reset my password")
            .removeClass("view-block")
            .addClass("view-hidden");
        $("#login-password-label").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-password").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-tab-login").css("font-weight", "normal");
        $("#login-tab-register").css("font-weight", "bold");
        $("#login-password-confirm-label").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-password-confirm").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-email-label").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-email").removeClass("view-hidden")
            .addClass("view-block");
        $("#login-status").removeClass("view-block")
            .addClass("view-hidden");
        login.type = "register";
    }

    function checkPasswords()
    {
        if(login.type == "register")
        {
            if($("#login-password").val() != $("#login-password-confirm").val())
            {
                login.password_valid = false;
                $("#login-status").text("Passwords do not match")
                    .removeClass("view-hidden")
                    .addClass("view-block");
            }
            else if($("#login-password").val() == "")
            {
                login.password_valid = false;
                $("#login-status").text("You must set a password.")
                    .removeClass("view-hidden")
                    .addClass("view-block");
            }
            else
            {
                login.password_valid = true;
                $("#login-status").removeClass("view-block")
                    .addClass("view-hidden");
            }
        }
    }

    function checkUsername()
    {
        if($("#login-username").val() == "" && login.type == "register")
        {
            console.log("User is empty");
            login.username_valid = false;
            $("#login-status").text("You must set a username.")
                .removeClass("view-hidden")
                .addClass("view-block");
        }
        else if(login.type == "register")
        {
            $("#login-status").removeClass("view-block")
                .addClass("view-hidden");
            login.username_valid = true;
        }
    }

    function addCheckerEvents()
    {
        $("#login-reset-password").click(function()
        {
            if(login.type == "login")
            {
                $("#form-type").val("reset");

                $("#login-status").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-password-label").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-password").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-submit-btn").text("Reset");
                $(this).text("I remembered my password");
                $("#login-tab-login").css("font-weight", "normal");
                $("#login-tab-register").css("font-weight", "normal");
                $("#login-password-confirm-label").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-password-confirm").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-email-label").removeClass("view-block")
                    .addClass("view-hidden");
                $("#login-email").removeClass("view-block")
                    .addClass("view-hidden");
                login.type = "reset";
            }
            else
            {
                openLogin();
            }
        });

        $("#login-tab-login").click(function()
        {
            openLogin();
        });

        $("#login-tab-register").click(function()
        {
            openRegister();
        });

        $("#login-username").change(function()
        {
            checkUsername();
        });

        $("#login-password").change(function()
        {
           checkPasswords();
        });

        $("#login-password-confirm").change(function()
        {
            checkPasswords();
        });

        $("#login-email").change(function()
        {
           checkEmail();
        });

        $("#login-submit-btn").click(function(e){
            e.preventDefault();
            checkLogin();
        });
    }

    $(document).ready(function()
    {
        $("#login-tab-login").css("font-weight", "bold");
        addCheckerEvents();
    });
}());
