(function(){

    var currentlySelected = null;

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function onListItemClicked()
    {
        currentlySelected = $(this).attr("category-id");

        $.ajax({url: "/api/v1/categories/"+$(this).attr("category-id"), success: function(result)
        {
            $("#categories-extended-view-edit").removeClass("view-hidden")
            .addClass("view-block");

            $("#categories-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");

            $("div.menu").children("a.list-group-item").removeClass("active");
            $(this).addClass("active");

            fill_data(result.category.top_expenses);

            $("#edit-title").val(result.category.name);
            $("#category-description").val(result.category.description);
        }});
    }

    function addCheckerEvents()
    {
        $(".status-hide").click(function()
        {
            $(this).parent().parent().removeClass("view-block").addClass("view-hidden");
        });

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $("div.menu > a.list-group-item").click(onListItemClicked);

        $(".categories-extended-view-hide").click(function()
        {
            currentlySelected = null;

            $("#categories-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");
            $("#categories-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");
            $("div.menu").children("a.list-group-item").removeClass("active");
        });

        $("#categories-new-category-btn").click(function()
        {
            $("#categories-extended-view-new").removeClass("view-hidden")
                .addClass("view-block");

            $("#categories-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");

            $("#new-title").val("");
            $("#category-description-new").val("");

            $("div.menu").children("a.list-group-item").removeClass("active");


        });

        $("#category-submit-btn").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            category_name = $("#edit-title").val();

            $.ajax({
                url: "/api/v1/categories/"+currentlySelected,
                type: "PUT",
                dataType: "json",
                contentType: 'application/json; charset=UTF-8',
                data: {
                    "name" : category_name,
                    "description" :  $("#category-description").val(),
                    "id" : currentlySelected,
                    "account_id" : $("#account-id").val()
                },
                complete: function(data, status){
                    if(status == "success")
                    {
                        if(data.responseJSON.status.code == "already_exists")
                        {
                            $("#category-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#category-status > span.category-status-message").html("Category with the name '" + category_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.responseJSON.status.code == "no_permission")
                        {
                            $("#category-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            ("#category-status > span.category-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.responseJSON.status.code == "success")
                        {
                            $("#category-status").removeClass("alert-warning").addClass("alert-success")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#category-status > span.category-status-message").html("Category '" + category_name + "' successfully updated.");
                            $("div.menu > a.list-group-item").filter("[category-id='"+currentlySelected+"']").text(category_name);
                        }
                    }
                    else
                    {
                        alert("Error while updating the database");
                    }
                }
            });

        });

        $("#category-delete-btn").click(function() {
            csrftoken = Cookies.get("csrftoken");
            category_name = $("#edit-title").val();

            $.ajax({
                url: "/api/v1/categories/" + currentlySelected,
                type: "DELETE",
                dataType: "json",
                data: {
                    "id": currentlySelected
                },
                complete: function (data, status) {
                    if (status == "success") {
                        $("#categories-extended-view-edit").removeClass("view-block")
                                .addClass("view-hidden");
                        $("div.menu > a.list-group-item").filter("[category-id='" + currentlySelected + "']").remove();
                        $("#category-status").removeClass("view-hidden").addClass("view-block");
                        $("#category-status > span.category-status-message").html("Category '" + category_name + "' successfully deleted.");
                        currentlySelected = null;
                    }
                    else {
                       $("#category-status").removeClass("alert-success").addClass("alert-warning")
                            .removeClass("view-hidden").addClass("view-block");
                        ("#category-status > span.category-status-message").html("You do not have the required permissions to do this.");
                    }
                }
            });
        });

        $("#category-submit-btn-new").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            category_name = $("#new-title").val().trim();

            if (category_name == "")
            {
                alert("Please enter a name for the category");
            }
            else
            {
                $.post("/api/v1/categories",
                {
                    "name" : category_name,
                    "description" :  $("#category-description-new").val(),
                    "account_id" : $("#account-id").val()
                },
                function(data, status){
                    if(status == "success")
                    {
                        if(data.status.code == "already_exists")
                        {
                            console.log("already exists");
                            $("#category-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#category-status > span.category-status-message").html("Category with the name '" + category_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.status.code == "no_permission")
                        {
                            $("#category-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#category-status > span.category-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.status.code == "success")
                        {
                            $("#category-status").removeClass("view-hidden").addClass("view-block");
                            $("#category-status > span.category-status-message").html("Category '" + category_name + "' successfully created.");
                            $("div.menu").append($("<a href=\"#\" category-id=\""+data.category.id+"\" class=\"list-group-item\">"+data.category.name+"</a>").click(onListItemClicked));

                            $("#new-title").val("");
                            $("#category-description-new").val("");
                        }
                    }
                    else
                    {
                        alert("Error while adding to the database");
                    }
                }
                );
            }
        });
    }
    $(document).ready(function()
    {
        addCheckerEvents();
    });
})();
