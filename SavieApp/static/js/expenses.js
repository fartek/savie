(function()
{
    var currentlySelected = null;
    var selectedCategory = null;
    var selectedCategoryEdit = null;

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function onListItemClicked()
    {
        currentlySelected = $(this).attr("expense-id");

        $.ajax({url: "/api/v1/expenses/"+$(this).attr("expense-id"), success: function(result)
        {
            $("#expenses-extended-view-edit").removeClass("view-hidden")
            .addClass("view-block");

            $("#expenses-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");

            $("div.menu").children("a.list-group-item").removeClass("active");
            $(this).addClass("active");

            $("#edit-title").val(result.expense.name);
            $("#edit-expense").val(result.expense.value);
            $("#categories-dropdown-edit > span.button-text").text(result.expense.category_name);
        }});
    }

    function updateExpensesVisuals(totalExpenses)
    {
        $("#total-expenses-value").text(totalExpenses);
        drawChart($("#account-id").val());
    }

    function addCheckerEvents()
    {
        selectedCategory = $("#categories-dropdown > span.button-text").text();
        selectedCategoryEdit = $("#categories-dropdown-edit > span.button-text").text();

        $(".status-hide").click(function()
        {
            $(this).parent().parent().removeClass("view-block").addClass("view-hidden");
        });

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $("#cat-dd > li").click(function(){
            var cat = $(this).text();
            $("#categories-dropdown > span.button-text").text(cat);
            selectedCategory = cat;
        });

        $("#cat-dd-edit > li").click(function(){
            var cat = $(this).text();
            $("#categories-dropdown-edit > span.button-text").text(cat);
            selectedCategoryEdit = cat;
        });

        $("div.menu > a.list-group-item").click(onListItemClicked);

        $("#expenses-new-expense-btn").click(function()
        {
            $("#expenses-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");

            $("#expenses-extended-view-new").removeClass("view-hidden")
                .addClass("view-block");

            $("div.menu").children("a.list-group-item").removeClass("active");
        });

        $(".expenses-extended-view-hide").click(function()
        {
            $("#expenses-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");
            $("#expenses-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");
            $("div.menu").children("a.list-group-item").removeClass("active");
        });

        $("#expense-submit-btn-new").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            expense_name = $("#new-title").val().trim();

            if (expense_name == "")
            {
                alert("Please enter a name for the expense");
            }
            else
            {
                $.post("/api/v1/expenses",
                {
                    "name" : expense_name,
                    "value" :  $("#new-expense").val(),
                    "account_id" : $("#account-id").val(),
                    "category_name" : selectedCategory
                },
                function(data, status){
                    if(status == "success")
                    {
                        if(data.status.code == "already_exists")
                        {
                            $("#expense-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#expense-status > span.expense-status-message").html("Expense with the name '" + expense_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.status.code == "no_permission")
                        {
                            $("#expense-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#expense-status > span.expense-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.status.code == "success")
                        {
                            var creation_date = Date.create(data.expense.created_at).format("{Mon}. {d}, {year}");

                            $("#new-title").val("");
                            $("#new-expense").val("1");

                            $("#expense-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                            $("#expense-status > span.expense-status-message").html("Expense '" + expense_name + "' successfully created.");
                            $("div.menu").append($("<a href=\"#manage\" expense-id=\""+data.expense.id+"\" class=\"list-group-item\"><span class=\"creation-date\">"+creation_date+"</span> <span class=\"expense-category\">&lt;"+data.expense.category_name.capitalize(true)+"&gt;</span> <span class=\"expense-name\">"+data.expense.name+"</span></a>").click(onListItemClicked));

                            updateExpensesVisuals(parseFloat($("#total-expenses-value").text())+parseFloat(data.expense.value));
                        }
                    }
                    else
                    {
                        alert("Error while adding to the database");
                    }
                }
                );
            }
        });

        $("#submit-edit").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            expense_name = $("#edit-title").val().trim();

            if (expense_name == "")
            {
                alert("Please enter a name for the expense");
            }
            else
            {
                $.ajax({
                url: "/api/v1/expenses/" + currentlySelected,
                type: "PUT",
                dataType: "json",
                data: {
                    "name" : expense_name,
                    "value" :  $("#edit-expense").val(),
                    "category_name" : selectedCategoryEdit,
                    "account_id" : $("#account-id").val()
                },
                complete: function (data, status) {
                    if(status == "success")
                    {
                        if(data.responseJSON.status.code == "already_exists")
                        {
                            $("#expense-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#expense-status > span.expense-status-message").html("Expense with the name '" + expense_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.responseJSON.status.code == "no_permission")
                        {
                            $("#expense-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#expense-status > span.expense-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.responseJSON.status.code == "success")
                        {
                            $("#expense-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                            $("#expense-status > span.expense-status-message").html("Expense '" + expense_name + "' successfully updated.");

                            $("div.menu > a.list-group-item").filter("[expense-id='"+currentlySelected+"']").find("span.expense-category").html("&lt;"+selectedCategoryEdit.capitalize(true)+"&gt;");
                            $("div.menu > a.list-group-item").filter("[expense-id='"+currentlySelected+"']").find("span.expense-name").text(expense_name);

                            var new_value = parseFloat($("#edit-expense").val());
                            var total_expenses_value = parseFloat($("#total-expenses-value").text());

                            alert(total_expenses_value+parseFloat(new_value)-parseFloat(data.responseJSON.expense.oldValue));

                            updateExpensesVisuals(total_expenses_value+parseFloat(new_value)-parseFloat(data.responseJSON.expense.oldValue));


                        }
                    }
                    else
                    {
                        alert("Error while editing the database");
                    }
                }
                });

            }
        });

        $(".monthly-budget").change(function(){
            var val = $(this).val();
            $(".monthly-budget").val(val);
        });

        $("#expenses-update-budget-btn").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            var monthly_budget = $(".monthly-budget").filter(":visible").val();


            if (monthly_budget == "")
            {
                alert("Please enter a monthly budget");
            }
            else
            {
                $.ajax({
                url: "/api/v1/expenses/",
                type: "PUT",
                dataType: "json",
                data: {
                    "monthly_budget" : monthly_budget,
                    "account_id" : $("#account-id").val()
                },
                complete: function (data, status) {
                    if(status == "success")
                    {
                        $("#monthly-budget-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                        $("#monthly-budget-status > span.monthly-budget-status-message").html("Monthly budget successfully updated");
                    }
                    else
                    {
                        $("#monthly-budget-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                        $("#monthly-budget-status > span.monthly-budget-status-message").html("Error while updating monthlu budget.");
                    }
                }
                });
            }
        });

        $("#submit-delete").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            expense_name = $("#edit-title").val().trim();

            $.ajax({
                url: "/api/v1/expenses/" + currentlySelected,
                type: "DELETE",
                dataType: "json",
                data: {
                    "id": currentlySelected
                },
                complete: function (data, status) {
                    if(status == "success")
                    {
                        $("#expenses-extended-view-edit").removeClass("view-block")
                                .addClass("view-hidden");

                        $("#expense-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                        $("#expense-status > span.expense-status-message").html("Expense '" + data.responseJSON.expense.name + "' successfully removed.");
                        $("div.menu > a.list-group-item").filter("[expense-id='"+currentlySelected+"']").remove();

                        updateExpensesVisuals(parseFloat($("#total-expenses-value").text())-parseFloat($("#edit-expense").val()));
                    }
                    else
                    {
                        $("#expense-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                        $("#expense-status > span.expense-status-message").html("You do not have the required permissions to do this.");
                    }
                }
            });
        });

    }

    $(document).ready(function()
    {
        addCheckerEvents();
        drawChart($("#account-id").val());
    });
})();
