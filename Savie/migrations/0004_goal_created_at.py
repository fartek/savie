# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-04 12:31
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Savie', '0003_goal'),
    ]

    operations = [
        migrations.AddField(
            model_name='goal',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 1, 4, 12, 31, 57, 402223, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
