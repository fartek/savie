from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone

from Savie.models import MoneyAccount, Expense, Category


def get_view(request, account_name):
    try:
        current_money_account = MoneyAccount.objects.get(savie_user=request.user.savieuser, name__iexact=account_name)
        other_money_accounts = MoneyAccount.objects.filter(savie_user = request.user.savieuser)

        expenses = Expense.objects.filter(category__money_account=current_money_account,
                  created_at__year=timezone.now().year, created_at__month=timezone.now().month)\
                    .order_by("created_at")
        categories = Category.objects.filter(money_account=current_money_account)
        default_category = categories[0] if categories.exists() else None

        monthly_budget = current_money_account.monthly_budget
        total_expenses = sum(e.value for e in expenses)

        context = {
            "savie_user" : request.user,
            "current_money_account" : current_money_account,
            "other_money_accounts" : other_money_accounts,
            "expenses" : expenses,
            "categories" : categories,
            "default_category" : default_category,
            "monthly_budget" : monthly_budget,
            "total_expenses" : total_expenses,
            "currency" : request.user.savieuser.currency,
        }
        return render(request, "expenses.html", context)
    except Exception:
        return HttpResponse("This account does not exist")