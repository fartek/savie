from datetime import date

from django.http import HttpResponse
from django.shortcuts import render

from Savie.models import MoneyAccount, Category


def get_view(request, account_name):
    try:
        current_money_account = MoneyAccount.objects.get(savie_user=request.user.savieuser, name__iexact=account_name)
        other_money_accounts = MoneyAccount.objects.filter(savie_user = request.user.savieuser)
        categories = Category.objects.filter(money_account=current_money_account)
        today = date.today()

        context = {
            "savie_user" : request.user,
            "current_money_account" : current_money_account,
            "other_money_accounts" : other_money_accounts,
            "categories" : categories,
            "current_month" : today.strftime("%B"),
            "current_year" : today.year,
        }
        return render(request, "categories.html", context)
    except Exception:
        return HttpResponse("This account does not exist")