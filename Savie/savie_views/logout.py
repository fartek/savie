from django.contrib.auth import logout
from django.http import HttpResponseRedirect

def get_view(request):
    if request.user.is_authenticated():
        logout(request)
    return HttpResponseRedirect("login")