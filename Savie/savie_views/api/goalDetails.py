import logging
from datetime import datetime

from django.utils import timezone

from django.http import JsonResponse, QueryDict

from Savie.models import Goal, MoneyAccount, Expense

calculate_percent_completed = lambda goal_to, today, goal_from : int((today.date()-goal_from).days/(goal_to-goal_from).days*100)

def get_goal_status(goal, total_spent):
    if goal.spending_max < total_spent:
        return "failed"
    elif timezone.now().date() >= goal.date_to:
        return "completed"
    else:
        return "in_progress"

def get_error_view(request, goal_id):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return JsonResponse({"status": {
        "message": "Error while accessing goal details api for goal id " + goal_id + ".",
        "code": "default_error"}})

def get_goal_progress(goal):
    today = timezone.now()
    total_spent = sum(e.value for e in Expense.objects.filter(category__money_account=goal.money_account))
    progress_status = get_goal_status(goal, total_spent)

    if progress_status == "failed" or progress_status == "completed":
        progress = {"status" : progress_status, "percent_completed" : 100}
    else:
        progress = {"status" : progress_status, "percent_completed" : calculate_percent_completed(goal.date_to, today, goal.date_from)}
    return progress

def get_GET_view(request, goal_id):
    try:
        goal = Goal.objects.get(id=goal_id, money_account__savie_user=request.user.savieuser)

        data = {"name": goal.name, "description": goal.description,
                "id": goal.id, "spending_max": goal.spending_max,
                "date_from" : goal.date_from, "date_to":goal.date_to,
                "created_at" : goal.created_at, "progress" : get_goal_progress(goal)}

        return JsonResponse({"goal": data})
    except Exception as e:
        print(e)
        return JsonResponse(
                {"status": {"message": "You don't have permission to access this object", "code": "no_permission"}})

def get_PUT_view(request, goal_id):
    data = QueryDict(request.body)

    name = data["name"]
    description = data["description"]
    account_id = data["account_id"]
    spending_max = data["spending_max"]
    date_from = data["date_from"]
    date_to = data["date_to"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        # if a goal with this name already exists
        if Goal.objects.filter(name__iexact=name, money_account=money_account).exclude(id=goal_id).exists():
            return JsonResponse({"status":{"message":"Updating unsuccessful. Goal name already exists.", "code":"already_exists"}})

        goal = Goal.objects.get(id=goal_id, money_account=money_account)

        goal.name = name
        goal.description = description
        goal.spending_max = spending_max
        goal.date_to = date_to
        goal.date_from = date_from
        goal.save()

        return JsonResponse({"goal" : { "id" : goal.id, "name" : goal.name }, "status":{"message":"Update successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to edit this object", "code":"no_permission"}})

def get_DELETE_view(request, goal_id):
    try:
        goal = Goal.objects.get(id=goal_id, money_account__savie_user=request.user.savieuser)
        goal_name = goal.name
        goal.delete()
        return JsonResponse({"goal":{"id" : goal_id, "name": goal_name},
                             "status":{"message":"Deletion of goal successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to delete this object", "code":"no_permission"}}, status=403)

def get_view(request, goal_id):
    response = {
        "GET": get_GET_view,
        "PUT": get_PUT_view,
        "DELETE": get_DELETE_view,
    }
    return response.get(request.method, get_error_view)(request, goal_id)
