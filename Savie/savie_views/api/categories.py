import logging
from datetime import timedelta
from django.http import JsonResponse
from Savie.models import MoneyAccount, Category, Expense
from django.utils import timezone


def get_GET_view(request):
    account_id = request.GET.get("money-account-id", None)
    categories = Category.objects.filter(money_account__savie_user=request.user.savieuser)

    since = request.GET.get("since-months", None)

    if account_id: # if the 'money-account-id' url param is set
        categories = categories.filter(money_account_id=account_id)

    json_categories = {"categories" : []}

    for category in categories:
        expenses = Expense.objects.filter(category__money_account__savie_user=request.user.savieuser, category=category)

        if since and since != "all":
            since_date = timezone.now()-timedelta(int(since)*30.5)
            since_date = since_date.replace(day=1)

            print("Since:", since_date)

            expenses = expenses.filter(created_at__gte = since_date)
        elif not since:
            expenses = expenses.filter(created_at__month=timezone.now().month, created_at__year=timezone.now().year)


        json_expenses = []

        for expense in expenses:
            json_expenses.append({
                "name" : expense.name,
                "value" : expense.value,
                "id" : expense.id,
                "created_at" : expense.created_at,
            })


        json_categories["categories"].append({
            "name" : category.name,
            "description" : category.description,
            "money_account_id" : category.money_account_id,
            "id" : category.id,
            "expenses" : json_expenses,
        })

    return JsonResponse(json_categories)

def get_POST_view(request):
    category_name = request.POST["name"]
    account_id = request.POST["account_id"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        if Category.objects.filter(money_account=money_account, name__iexact=category_name).exists():
            return JsonResponse({"status":{"message":"creation unsuccessful. This category name already exists", "code":"already_exists"}})
        else:
            category_description = request.POST["description"]
            new_category = Category.objects.create(name=category_name, description=category_description, money_account=money_account)
            return JsonResponse({"category":{"name":new_category.name,
                    "description":new_category.description,"id":new_category.id},
                    "status":{"message":"Category creation successful.", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You do not have the required permissions.", "code":"no_permission"}})

def get_error_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return JsonResponse({"status":{"message":"Error while accessing categories api.", "code":"default_error"}})

def get_view(request):
    response = {
        "GET" : get_GET_view,
        "POST" : get_POST_view,
    }
    return response.get(request.method, get_error_view)(request)