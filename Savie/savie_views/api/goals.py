import logging

from django.http import JsonResponse

from Savie.models import Goal, MoneyAccount, Expense
from Savie.savie_views.api.goalDetails import get_goal_status, calculate_percent_completed, get_goal_progress


def get_error_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return JsonResponse({"status":{"message":"Error while accessing goals api.", "code":"default_error"}})

def get_GET_view(request):
    try:
        goals = Goal.objects.filter(money_account__savie_user=request.user.savieuser)
        json_goals = {"goals" : []}

        for goal in goals:
            json_goals["goals"].append({
                "name" : goal.name,
                "description" : goal.description,
                "spending_max" : goal.spending_max,
                "date_from" : goal.date_from,
                "date_to" : goal.date_to,
                "created_at" : goal.created_at,
                "id" : goal.id,
                "progress" : get_goal_progress(goal),
            })

        return JsonResponse(json_goals)
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You do not have the required permissions.", "code":"no_permission"}})

def get_POST_view(request):
    name = request.POST["name"]
    description = request.POST["description"]
    spending_max = request.POST["spending_max"]
    date_from = request.POST["date_from"]
    date_to = request.POST["date_to"]
    account_id = request.POST["account_id"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        if Goal.objects.filter(money_account=money_account, name__iexact=name).exists():
            return JsonResponse({"status":{"message":"creation unsuccessful. Goal name already exists.", "code":"already_exists"}})
        else:
            new_goal = Goal.objects.create(name=name, description=description, spending_max=spending_max,
                       date_from=date_from, date_to=date_to, money_account=money_account)
            return JsonResponse({"goal":{"name":new_goal.name,
                    "description":new_goal.description,"id":new_goal.id,
                    "spending_max":new_goal.spending_max,
                    "date_from":new_goal.date_from, "date_to" : new_goal.date_to},
                    "status":{"message":"Goal creation successful.", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You do not have the required permissions.", "code":"no_permission"}})

def get_view(request):
    response = {
        "GET" : get_GET_view,
        "POST" : get_POST_view,
    }
    return response.get(request.method, get_error_view)(request)