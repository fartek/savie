import logging

from django.http import JsonResponse, QueryDict

from Savie.models import Expense, MoneyAccount, Category


def get_GET_view(request, expense_id):
    try:
        expense = Expense.objects.get(id=expense_id, category__money_account__savie_user=request.user.savieuser)
        data = {"name": expense.name, "value": expense.value,
                "id": expense.id, "category_id": expense.category.id,
                "category_name" : expense.category.name, "created_at":expense.created_at}

        return JsonResponse({"expense": data})
    except Exception as e:
        print(e)
        return JsonResponse(
                {"status": {"message": "You don't have permission to access this object", "code": "no_permission"}})


def get_PUT_view(request, expense_id):
    data = QueryDict(request.body)

    expense_name = data["name"]
    expense_value = data["value"]
    account_id = data["account_id"]
    category_name = data["category_name"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        # if expense with this name already exists
        if Expense.objects.filter(category__name__iexact=category_name, category__money_account=money_account, name__iexact=expense_name).exclude(id=expense_id).exists():
            return JsonResponse({"status":{"message":"Updating unsuccessful. Expense name already exists in this category", "code":"already_exists"}})

        category = Category.objects.get(name__iexact=category_name, money_account=money_account)

        expense = Expense.objects.get(id=expense_id, category__money_account=money_account)

        old_value = expense.value

        expense.name = expense_name
        expense.value = expense_value
        expense.category = category
        expense.save()

        return JsonResponse({"expense" : { "id" : expense_id, "old_value" : old_value }, "status":{"message":"Update successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to edit this object", "code":"no_permission"}})


def get_DELETE_view(request, expense_id):
    try:
        expense = Expense.objects.get(id=expense_id, category__money_account__savie_user=request.user.savieuser)
        expense_name = expense.name
        expense.delete()
        return JsonResponse({"expense":{"id" : expense_id, "name": expense_name},
                             "status":{"message":"Deletion of expense successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to delete this object", "code":"no_permission"}}, status=403)


def get_error_view(request, expense_id):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return JsonResponse({"status": {
        "message": "Error while accessing expense details api for expense id " + expense_id + ".",
        "code": "default_error"}})


def get_view(request, expense_id):
    response = {
        "GET": get_GET_view,
        "PUT": get_PUT_view,
        "DELETE": get_DELETE_view,
    }
    return response.get(request.method, get_error_view)(request, expense_id)
