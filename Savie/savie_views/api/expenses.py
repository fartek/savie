from django.utils import timezone
import logging

from django.http import JsonResponse, QueryDict

from Savie.models import Expense, MoneyAccount, Category


def get_GET_view(request):

    account_id = request.GET.get("money-account-id", None)
    category_id = request.GET.get("category-id", None)
    month = request.GET.get("month", None)

    expenses = Expense.objects.filter(category__money_account__savie_user=request.user.savieuser)

    if account_id: # if the 'money-account-id' url param is set
        expenses = expenses.filter(category__money_account=account_id)

    if category_id:
        expenses = expenses.filter(category=category_id)

    if month:
        expenses = expenses.filter(created_at__month=month, created_at__year=timezone.now().year)

    json_expenses = {"expenses" : []}

    for expense in expenses:
        json_expenses["expenses"].append({
            "name" : expense.name,
            "value" : expense.value,
            "id" : expense.id,
            "category_name" : expense.category.name,
            "created_at" : expense.created_at,
        })

    return JsonResponse(json_expenses)

def get_POST_view(request):
    expense_name = request.POST["name"]
    expense_value = request.POST["value"]
    category_name = request.POST["category_name"]
    account_id = request.POST["account_id"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        category = Category.objects.get(money_account=money_account, name__iexact=category_name)
        if Expense.objects.filter(category=category, name__iexact=expense_name).exists():
            return JsonResponse({"status":{"message":"creation unsuccessful. Expense name already exists in this category", "code":"already_exists"}})
        else:
            new_expense = Expense.objects.create(name=expense_name, value=expense_value, category=category)
            return JsonResponse({"expense":{"name":new_expense.name,
                    "value":new_expense.value,"id":new_expense.id,
                    "category_name":new_expense.category.name,
                    "created_at":new_expense.created_at},
                    "status":{"message":"Expense creation successful.", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You do not have the required permissions.", "code":"no_permission"}})

def get_PUT_view(request):
    data = QueryDict(request.body)
    monthly_budget = data.get("monthly_budget", None)
    account_id = data["account_id"]

    if monthly_budget:
        try:
            money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
            money_account.monthly_budget = monthly_budget
            money_account.save()
        except Exception as e:
            print(e)

    return JsonResponse({"updated":[{"name":"monthly budget", "value":monthly_budget}]})

def get_error_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)

    return JsonResponse({"status":{"message":"Error while accessing expenses api.", "code":"default_error"}})

def get_view(request):
    response = {
        "GET" : get_GET_view,
        "POST" : get_POST_view,
        "PUT" : get_PUT_view,
    }
    return response.get(request.method, get_error_view)(request)