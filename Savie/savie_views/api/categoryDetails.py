import logging
from django.utils import timezone

from django.http import JsonResponse, QueryDict

from Savie.models import Category, MoneyAccount, Expense


def get_GET_view(request, category_id):
    try:
        today = timezone.now()
        category = Category.objects.get(id=category_id, money_account__savie_user=request.user.savieuser)
        expenses = Expense.objects.filter(category=category, created_at__year=today.year, created_at__month=today.month).order_by("-value")[:10]

        top_expenses = [{"name":e.name, "value":e.value} for e in expenses]

        data = {"name" : category.name, "description" : category.description,
            "id" : category.id, "money_account_id" : category.money_account_id,
                "top_expenses" : top_expenses}

        return JsonResponse({"category" : data})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to access this object", "code":"no_permission"}})


def get_PUT_view(request, category_id):
    data = QueryDict(request.body)

    name = data["name"]
    description = data["description"]
    account_id = data["account_id"]

    try:
        money_account = MoneyAccount.objects.get(id=account_id, savie_user=request.user.savieuser)
        # if category with this name already exists
        if Category.objects.filter(money_account=money_account, name__iexact=name).exclude(id=category_id).exists():
            return JsonResponse({"status":{"message":"Updating unsuccessful. This category name already exists", "code":"already_exists"}})

        category = Category.objects.get(id=category_id, money_account__savie_user=request.user.savieuser)
        category.name = name
        category.description = description
        category.save()
        return JsonResponse({"category" : { "id" : category_id }, "status":{"message":"Update successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to edit this object", "code":"no_permission"}})

def get_DELETE_view(request, category_id):
    try:
        category = Category.objects.get(id=category_id, money_account__savie_user=request.user.savieuser)
        category.delete()
        return JsonResponse({"category":{"id" : category_id},
                             "status":{"message":"Deletion of category successful", "code":"success"}})
    except Exception as e:
        print(e)
        return JsonResponse({"status":{"message":"You don't have permission to delete this object", "code":"no_permission"}}, status=403)

def get_error_view(request, category_id):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return JsonResponse({"status":{"message":"Error while accessing category details api for category id " + category_id +".", "code":"default_error"}})

def get_view(request, category_id):
    response = {
        "GET" : get_GET_view,
        "PUT" : get_PUT_view,
        "DELETE" : get_DELETE_view,
    }
    return response.get(request.method, get_error_view)(request, category_id)