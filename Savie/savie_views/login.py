from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from Savie.models import SavieUser, MoneyAccount
import logging

__author__ = 'fartek'

render_login = lambda request, context : render(request, "login.html", context)

def login_and_redirect(user, request):
    login(request, user)
    # Redirect to a success page.
    next_url = request.POST.get("next", None)
    if next_url:
        return HttpResponseRedirect(next_url)
    else:
        return HttpResponseRedirect(reverse("default"))

def handle_user_login(request):
    username = request.POST["username"]
    password = request.POST["password"]

    logger = logging.getLogger(__name__)

    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            return login_and_redirect(user, request)
        else:
            # Return a 'disabled account' error message
            context = {"invalid_login" : True, "login_status" : "This account is disabled"}
            return render_login(request, context)
    else:
        # Return an 'invalid login' error message.
        context = {"invalid_login" : True, "login_status" : "Invalid username or password"}
        logger.warning("Invalid login from username '" + username + "' - invalid username or password")
        return render_login(request, context)

def handle_user_register(request):
    username = request.POST["username"]
    password = request.POST["password"]
    password_confirm = request.POST["password-confirm"]
    email = request.POST["email"]
    currency = request.POST["currency"]

    logger = logging.getLogger(__name__)

    if username == "" or password == "" or password_confirm == "":
        context = {"invalid_login" : True, "login_status" : "Error while registering: Please fill out all the fields."}
        logger.warning("Invalid registration from username '" + username + "' - not all fields are filled out")
        return render_login(request, context)
    else:
        if User.objects.filter(username=username).exists(): # Check if username already exists
            context = {"invalid_login" : True, "login_status" : "Error while registering: A user with this username already exists."}
            logger.warning("Invalid registration from username '" + username + "' - username already exists")
            return render_login(request, context)
        else:
            print("user",username,"does not yet exist. Registering it now...")
            if password == password_confirm:
                user = User.objects.create_user(username, email, password)
                SavieUser.objects.create(user=user, currency=currency)
                MoneyAccount.objects.create(name="bank", savie_user=user.savieuser, monthly_budget=1000)

                #authenticate the user, log them in and redirect them
                user = authenticate(username=username, password=password)
                if user is not None and user.is_active:
                    return login_and_redirect(user, request)
                else:
                    return get_error_view(request)
            else:
                # passwords do not match
                context = {"invalid_login" : True, "login_status" : "Error while registering: Passwords do not match."}
                logger.warning("Invalid registration from username '" + username + "' - passwords do not match")
                return render_login(request, context)

def get_GET_view(request):
    if not request.user.is_authenticated():
        context = {
            "next" : request.GET.get("next", None)
        }
        return render_login(request, context)
    else:
        return HttpResponseRedirect(reverse("default"))

def get_error_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return HttpResponse("Error while accessing the login page.")

def get_POST_view(request):
    response = {
        "login" : handle_user_login,
        "register" : handle_user_register,
    }
    return response.get(request.POST["form-type"], get_error_view)(request)


def get_view(request):

    response = {
        "GET" : get_GET_view,
        "POST" : get_POST_view,
    }
    return response.get(request.method, get_error_view)(request)