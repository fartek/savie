import logging

from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.shortcuts import render

from Savie.models import MoneyAccount

def get_GET_view(request):
    all_money_accounts = MoneyAccount.objects.filter(savie_user=request.user.savieuser)
    default_money_account = all_money_accounts[:1].get()
    context = {
        "user" : request.user,
        "current_money_account":default_money_account,
        "all_money_accounts" :  all_money_accounts,
    }
    return render(request, "settings.html", context)

def get_POST_view(request):
    print("Updating user:", request.user)
    all_money_accounts = MoneyAccount.objects.filter(savie_user=request.user.savieuser)

    new_email = request.POST["new-email"]
    new_email_confirm = request.POST["new-email-confirm"]
    password = request.POST["password"]
    new_password = request.POST["new-password"]
    new_password_confirm = request.POST["new-password-confirm"]
    currency = request.POST["currency"]
    modified_money_accounts = request.POST.getlist("money-account-names[]")


    accounts_to_remove = []
    accounts_to_add = []

    for money_account in all_money_accounts:
        if money_account.name.lower() not in modified_money_accounts:
            accounts_to_remove.append(money_account)

    for money_account in modified_money_accounts:
        if money_account.lower() not in [a.name.lower() for a in all_money_accounts]:
            accounts_to_add.append(money_account)

    for account in accounts_to_add:
        MoneyAccount.objects.create(name=account, savie_user=request.user.savieuser)

    for account in accounts_to_remove:
        logger = logging.getLogger(__name__)
        try:
            if MoneyAccount.objects.filter(savie_user=request.user.savieuser).count() <= 1:
                logger.warning("User",request.user.savieuser,"tried to delete their last savie account")
            else:
                account.delete()
        except Exception as e:
            logger.error("User", request.user.savieuser.name, " - Error while deleting money account", account.name)
            logger.exception(e)

    all_money_accounts = MoneyAccount.objects.filter(savie_user=request.user.savieuser)
    default_money_account = all_money_accounts[:1].get()

    emails_match = True
    valid_password = True
    new_passwords_match = True
    changed_email = False
    changed_password = False
    changed_currency = False

    if new_email != "": # user is changing email
        if new_email == new_email_confirm:
            changed_email = True
            request.user.email = new_email

    if password != "" or new_password != "" or new_password_confirm != "":
        changed_password = True

    if request.user.check_password(password): # user entered their 'current password' correctly
        print("current password is correct")
        if new_password == new_password_confirm: # new password and confirmation new passwords match
            request.user.set_password(new_password)
        else:
            new_passwords_match = False
    elif password != "" or new_password != "" or new_password_confirm != "":
        valid_password = False

    if currency != "": # user changed the currency
        request.user.savieuser.currency = currency[:3].upper()
        request.user.savieuser.save()
        changed_currency = True

    request.user.save()
    if changed_password and valid_password and new_passwords_match: #relog the user in
        user = authenticate(username=request.user, password=new_password)
        if user is not None:
            login(request, user)

    context = {
        "current_money_account":default_money_account,
        "user" : request.user,
        "emails_match" : emails_match,
        "valid_password" : valid_password,
        "new_passwords_match" : new_passwords_match,
        "changed_email" : changed_email,
        "changed_password" : changed_password,
        "changed_currency" : changed_currency,
        "all_money_accounts" :  all_money_accounts,
    }

    return render(request, "settings.html", context)

def get_error_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Invalid request method:", request.method)
    return HttpResponse("Error while accessing settings.")

def get_view(request):
    response = {
        "GET" : get_GET_view,
        "POST" : get_POST_view,
    }
    return response.get(request.method, get_error_view)(request)