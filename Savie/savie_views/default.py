import logging

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from Savie.models import MoneyAccount

def get_view(request):
    logger = logging.getLogger(__name__)
    logger.info("Redirecting from '" + request.get_full_path() + "' to default page")

    default_money_account = MoneyAccount.objects.filter(savie_user = request.user.savieuser)[:1].get()
    return HttpResponseRedirect(reverse("overview", kwargs={"account_name" : default_money_account.name}))