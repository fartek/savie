from datetime import datetime, timedelta

from django.http import HttpResponse
from django.shortcuts import render

from Savie.models import MoneyAccount, Expense, Goal
from Savie.savie_views.api.goalDetails import get_goal_progress

from django.utils import timezone


def get_view(request, account_name):
    try:
        current_money_account = MoneyAccount.objects.get(savie_user= request.user.savieuser, name__iexact=account_name)
        other_money_accounts = MoneyAccount.objects.filter(savie_user = request.user.savieuser)

        expenses = Expense.objects.filter(category__money_account=current_money_account,
                                          created_at__month=timezone.now().month,
                                          created_at__year=timezone.now().year)
        monthly_budget = current_money_account.monthly_budget
        total_expenses = sum(e.value for e in expenses)

        goals = Goal.objects.filter(money_account=current_money_account,
                date_to__range=(datetime.now()-timedelta(days=5), datetime.now()+timedelta(days=60)))

        for goal in goals:
            goal.progress = get_goal_progress(goal)

        currency = request.user.savieuser.currency

        context = {
            "user" : request.user,
            "current_money_account" : current_money_account,
            "other_money_accounts" : other_money_accounts,
            "monthly_budget" : monthly_budget,
            "total_expenses" : total_expenses,
            "current_month" : timezone.now().strftime("%B"),
            "current_year" : timezone.now().year,
            "goals" : goals,
            "currency": currency,
        }

        return render(request, "index.html", context)
    except Exception as e:
        print(e)
        return HttpResponse("This account does not exist")