from django.conf.urls import url

from Savie import views

urlpatterns = [
    url(r'^accounts/(?P<account_name>[a-zA-Z0-9_\-]{1,128})/overview$',
        views.overview_view, name='overview'),
    url(r'^settings$', views.settings_view, name="settings"),
    url(r'^login$', views.login_view, name="login"),
    url(r'^logout$', views.logout_view, name="logout"),
    url(r'^accounts/(?P<account_name>[a-zA-Z0-9_\-]{1,128})/categories$', views.categories_view, name="categories"),
    url(r'^accounts/(?P<account_name>[a-zA-Z0-9_\-]{1,128})/expenses$', views.expenses_view, name="expenses"),
    url(r'^accounts/(?P<account_name>[a-zA-Z0-9_\-]{1,128})/goals$', views.goals_view, name="goals"),

    url(r'^api/v1/categories/(?P<category_id>\d+)/?$', views.api_category_details_view, name="api_category_details"),
    url(r'^api/v1/categories/?$', views.api_categories_view, name="api_categories"),

    url(r'^api/v1/expenses/(?P<expense_id>\d+)/?$', views.api_expense_details_view, name="api_expense_details"),
    url(r'^api/v1/expenses/?$', views.api_expenses_view, name="api_expenses"),

    url(r'^api/v1/goals/(?P<goal_id>\d+)/?$', views.api_goal_details_view, name="api_goal_details"),
    url(r'^api/v1/goals/?$', views.api_goals_view, name="api_goals"),

    url(r'^', views.default_view, name="default"),
]