from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from Savie.savie_views import overview, settings, login, logout, default,\
    categories, api, expenses, goals

@login_required
def default_view(request):
    return default.get_view(request)

@login_required
def overview_view(request, account_name):
    return overview.get_view(request, account_name)

@login_required
def settings_view(request):
    return settings.get_view(request)

@login_required
def categories_view(request, account_name):
    return categories.get_view(request, account_name)

@login_required
def expenses_view(request, account_name):
    return expenses.get_view(request, account_name)

@login_required
def goals_view(request, account_name):
    return goals.get_view(request, account_name)


# API

@login_required
def api_categories_view(request):
    return api.categories.get_view(request)

@login_required
def api_category_details_view(request, category_id):
    return api.categoryDetails.get_view(request, category_id)


@login_required
def api_expenses_view(request):
    return api.expenses.get_view(request)

@login_required
def api_expense_details_view(request, expense_id):
    return api.expenseDetails.get_view(request, expense_id)


@login_required
def api_goals_view(request):
    return api.goals.get_view(request)

@login_required
def api_goal_details_view(request, goal_id):
    return api.goalDetails.get_view(request, goal_id)


# Login

def login_view(request):
    return login.get_view(request)

def logout_view(request):
    return logout.get_view(request)








