var data;

$(window).resize(function()
{
    drawChart();
});

function fill_data(expenses)
{
    // Create the data table.
    data = new google.visualization.DataTable();
    data.addColumn('string', 'Expense title');
    data.addColumn('number', 'Expense');

    var rows = [];

    for(var i=0;i<expenses.length;i++)
    {
        var e = expenses[i];
        rows.push([e["name"], e["value"]])
    }

    data.addRows(rows);

    drawChart();
}




// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
//google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart()
{

    w = document.getElementById("category-graph-container").offsetWidth-30;

    // Set chart options
    var options = {
        'width':w,
        'height':w/1.5};

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}
