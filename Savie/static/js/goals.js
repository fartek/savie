(function(){

    var currentlySelected = null;


    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function updateProgressBar(goal_id)
    {
        $.ajax({url: "/api/v1/goals/"+goal_id, success: function(result)
        {
            var progress_bar = $("#goal-progress-bar");
            if(result.goal.progress.status == "failed")
            {
                progress_bar.removeClass("progress-bar-info progress-bar-success").addClass("progress-bar-danger");
                progress_bar.attr("aria-valuenow", "100");
                progress_bar.css("width", "100%");
                $("#goal-progress-bar-text").text("Failed");
            }
            else if(result.goal.progress.status == "completed")
            {
                progress_bar.removeClass("progress-bar-info progress-bar-danger").addClass("progress-bar-success");
                progress_bar.attr("aria-valuenow", "100");
                progress_bar.css("width", "100%");
                $("#goal-progress-bar-text").text("Goal completed");
            }
            else
            {
                progress_bar.removeClass("progress-bar-success progress-bar-danger").addClass("progress-bar-info");
                progress_bar.attr("aria-valuenow", result.goal.progress.percent_completed);
                progress_bar.css("width", result.goal.progress.percent_completed+"%");
                $("#goal-progress-bar-text").text("Completed " + result.goal.progress.percent_completed + "%");
            }
        }});

    }

    function onListItemClicked()
    {
        currentlySelected = $(this).attr("goal-id");

        $.ajax({url: "/api/v1/goals/"+currentlySelected, success: function(result)
        {
            $("#goals-extended-view-edit").removeClass("view-hidden")
            .addClass("view-block");

            $("#goals-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");

            $("div.menu").children("a.list-group-item").removeClass("active");
            $(this).addClass("active");

            $("#edit-title").val(result.goal.name);
            $("#description-edit").val(result.goal.description);
            $("#time-frame-from-edit").val(result.goal.date_from);
            $("#time-frame-to-edit").val(result.goal.date_to);
            $("#goal-budget-edit").val(result.goal.spending_max);

            updateProgressBar(currentlySelected);
        }});
    }

    function addCheckerEvents()
    {
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $(".status-hide").click(function()
        {
            $(this).parent().parent().removeClass("view-block").addClass("view-hidden");
        });

        $("div.menu > a.list-group-item").click(onListItemClicked);

        $(".goals-extended-view-hide").click(function()
        {
            $("#goals-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");
            $("#goals-extended-view-new").removeClass("view-block")
                .addClass("view-hidden");
            $("div.menu").children("a.list-group-item").removeClass("active");
        });

        $("#goals-new-goal-btn").click(function()
        {
            $("#goals-extended-view-new").removeClass("view-hidden")
                .addClass("view-block");

            $("#goals-extended-view-edit").removeClass("view-block")
                .addClass("view-hidden");

            $("div.menu").children("a.list-group-item").removeClass("active");
        });

        $(".date-input").change(function()
        {
            var inputs = $(".date-input");
            var edit_from = inputs[0];
            var edit_to = inputs[1];

            var new_from = inputs[2];
            var new_to = inputs[3];

            if(typeof edit_to.value != 'undefined' && typeof edit_from.value != 'undefined' && edit_to.value < edit_from.value)
            {
                $(".goals-date-status").removeClass("view-hidden").addClass("view-block");
                $(this).val("");
            }
            else
            {
                $(".goals-date-status").removeClass("view-block").addClass("view-hidden");

                if(typeof new_from.value != 'undefined' && typeof new_to.value != 'undefined'
                    && $.trim(new_from.value) != "" && $.trim(new_to.value) != "" && new_to.value < new_from.value)
                {
                    console.log("'",new_from.value,"', '", new_to.value, "'");
                    $(".goals-date-status").removeClass("view-hidden").addClass("view-block");
                    $(this).val("");
                }
                else
                {
                    console.log('2else');
                    $(".goals-date-status").removeClass("view-block").addClass("view-hidden");
                }
            }
        });

        $("#goal-submit-btn-new").click(function(e){
            e.preventDefault();
            csrftoken = Cookies.get("csrftoken");
            goal_name = $("#new-title").val().trim();

            if (goal_name == "")
            {
                alert("Please enter a name for the expense");
            }
            else if($("#time-frame-from-new").val() == "")
            {
                alert("Please enter a date from which the goal will start");
            }
            else if($("#time-frame-to-new").val() == "")
            {
                alert("Please enter a date at which the goal will end");
            }
            else if($("#goal-budget-edit").val() == "")
            {
                alert("Please enter a goal spending maximum.")
            }
            else
            {
                $.post("/api/v1/goals",
                {
                    "name" : goal_name,
                    "description" :  $("#description-new").val(),
                    "account_id" : $("#account-id").val(),
                    "spending_max": $("#goal-budget-new").val(),
                    "date_from" : $("#time-frame-from-new").val(),
                    "date_to" : $("#time-frame-to-new").val()
                },
                function(data, status){
                    if(status == "success")
                    {
                        if(data.status.code == "already_exists")
                        {
                            $("#goal-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#goal-status > span.goal-status-message").html("Goal with the name '" + goal_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.status.code == "no_permission")
                        {
                            $("#goal-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#goal-status > span.goal-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.status.code == "success")
                        {
                            $("#new-title").val("");
                            $("#description-new").val("");
                            $("#goal-budget-new").val("1000");

                            var today = Date.create();

                            $("#time-frame-from-new").val(today.format("{year}-{MM}-{dd}"));
                            $("#time-frame-to-new").val(today.format("{year}-{MM}-{dd}"));

                            $("#goal-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                            $("#goal-status > span.goal-status-message").html("Goal '" + goal_name + "' successfully created.");
                            $("div.menu").append($("<a href=\"#\" goal-id=\""+data.goal.id+"\" class=\"list-group-item\">"+data.goal.name+"</a>").click(onListItemClicked));
                        }
                    }
                    else
                    {
                        alert("Error while adding to the database");
                    }
                }
            );
            }
        });

        $("#goal-submit-btn-edit").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            goal_name = $("#edit-title").val().trim();

            if (goal_name == "")
            {
                alert("Please enter a name for the expense");
            }
            else if($("#time-frame-from-edit").val() == "")
            {
                alert("Please enter a date from which the goal will start");
            }
            else if($("#time-frame-to-edit").val() == "")
            {
                alert("Please enter a date at which the goal will end");
            }
            else if($("#goal-budget-edit").val() == "")
            {
                alert("Please enter a goal spending maximum.")
            }
            else
            {
                $.ajax({
                url: "/api/v1/goals/" + currentlySelected,
                type: "PUT",
                dataType: "json",
                data: {
                    "name" : goal_name,
                    "description" : $("#description-edit").val(),
                    "date_from" :  $("#time-frame-from-edit").val(),
                    "date_to" :  $("#time-frame-to-edit").val(),
                    "spending_max" :  $("#goal-budget-edit").val(),
                    "account_id" : $("#account-id").val()
                },
                complete: function (data, status) {
                    if(status == "success")
                    {
                        if(data.responseJSON.status.code == "already_exists")
                        {
                            $("#goal-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#goal-status > span.goal-status-message").html("Goal with the name '" + goal_name.toLowerCase() + "' already exists.");
                        }
                        else if(data.responseJSON.status.code == "no_permission")
                        {
                            $("#goal-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                            $("#goal-status > span.goal-status-message").html("You do not have the required permissions to do this.");
                        }
                        else if(data.responseJSON.status.code == "success")
                        {
                            $("#goal-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                            $("#goal-status > span.goal-status-message").html("Goal '" + goal_name.capitalize() + "' successfully updated.");

                            $("div.menu > a.list-group-item").filter("[goal-id='"+currentlySelected+"']").text(goal_name.capitalize());
                            updateProgressBar(currentlySelected);
                        }
                    }
                    else
                    {
                        alert("Error while editing the database");
                    }
                }
                });
            }
        });

        $("#goal-delete-btn-edit").click(function()
        {
            csrftoken = Cookies.get("csrftoken");
            goal_name = $("#edit-title").val().trim();

            $.ajax({
                url: "/api/v1/goals/" + currentlySelected,
                type: "DELETE",
                dataType: "json",
                data: {
                    "id": currentlySelected
                },
                complete: function (data, status) {
                    if(status == "success")
                    {
                        $("#goals-extended-view-edit").removeClass("view-block")
                                .addClass("view-hidden");

                        $("#goal-status").removeClass("view-hidden").addClass("view-block")
                            .removeClass("alert-warning").addClass("alert-success");
                        $("#goal-status > span.goal-status-message").html("Goal '" + data.responseJSON.goal.name + "' successfully removed.");
                        $("div.menu > a.list-group-item").filter("[goal-id='"+currentlySelected+"']").remove();
                    }
                    else
                    {
                        $("#goal-status").removeClass("alert-success").addClass("alert-warning")
                                .removeClass("view-hidden").addClass("view-block");
                        $("#goal-status > span.expense-status-message").html("You do not have the required permissions to do this.");
                    }
                }
            });
        });

    }

    $(document).ready(function()
    {
        addCheckerEvents();
    });
})();
