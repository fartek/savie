$(window).resize(function()
{
    drawChart($("#account-id").val());
});

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
//google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart(account_id)
{
    var this_month = Date.create().format("{M}");
    $.getJSON( "/api/v1/expenses?money-account-id="+account_id+"&month="+this_month, function( result ) {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Expense title');
        data.addColumn('number', 'Expense');

        var data_rows = [];

        for(var i=0;i<result.expenses.length;i++)
        {
            var expense = result.expenses[i];
            var expense_name = "<"+expense.category_name+"> " + expense.name;
            data_rows.push([expense_name, parseFloat(expense.value)]);
        }

        data.addRows(data_rows);

        w = document.getElementById("category-graph-container").offsetWidth-30;

        // Set chart options
        var options = {
            'width':w,
            'height':w/1.5};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    });
}
