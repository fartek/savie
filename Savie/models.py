from django.contrib.auth.models import User
from django.db import models

class SavieUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    currency = models.CharField(max_length=3)

class MoneyAccount(models.Model):
    name = models.CharField(max_length=128)
    savie_user = models.ForeignKey(SavieUser, on_delete=models.CASCADE)
    monthly_budget = models.PositiveIntegerField(default=1000)

class Category(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField()
    money_account = models.ForeignKey(MoneyAccount, on_delete=models.CASCADE)

class Expense(models.Model):
    name = models.CharField(max_length=128)
    value = models.PositiveIntegerField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

class Goal(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField()
    spending_max = models.PositiveIntegerField()
    date_from = models.DateField()
    date_to = models.DateField()
    money_account = models.ForeignKey(MoneyAccount, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)